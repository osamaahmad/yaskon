. scripts/venv/create_venv.sh
. scripts/venv/install_requirements.sh
. scripts/docker/build_image.sh
. scripts/docker/run.sh
