from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.auth.base_user import BaseUserManager

from core.functions import UploadImage

from .settings import MAX_NAME_LENGTH,\
    MAX_USERNAME_LENGTH,\
    MAX_EMAIL_LENGTH,\
    PROFILE_PICTURES_UPLOAD_PATH


get_profile_picture_upload_path = UploadImage(upload_path=PROFILE_PICTURES_UPLOAD_PATH)


class UserManager(BaseUserManager):

    def construct_user(self, username, email, password=None, **extra_fields):
        raise_value_error_if_empty(username, 'username')
        raise_value_error_if_empty(email, 'email')
        normalized_email = self.normalize_email(email)
        user = self.model(username=username, email=normalized_email, **extra_fields)
        user.set_password(password)
        return user

    def create_user(self, username, email, password=None, **extra_fields):
        user = self.construct_user(username, email, password, **extra_fields)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password, **extra_fields):
        user = self.construct_user(username=username, email=email, password=password, **extra_fields)
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class User(PermissionsMixin, AbstractBaseUser):

    def __str__(self):
        return f'{self.first_name} {self.last_name} ({self.username})'

    first_name = models.CharField(max_length=MAX_NAME_LENGTH, blank=False)
    last_name = models.CharField(max_length=MAX_NAME_LENGTH, blank=False)
    username = models.CharField(unique=True, max_length=MAX_USERNAME_LENGTH, blank=False)
    email = models.EmailField(unique=True, max_length=MAX_EMAIL_LENGTH, blank=False)
    profile_picture = models.ImageField(upload_to=get_profile_picture_upload_path, blank=True, null=True)
    is_broker = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    REQUIRED_FIELDS = ['first_name', 'last_name', 'email']
    USERNAME_FIELD = 'username'

    objects = UserManager()


def raise_value_error_if_empty(field, name):
    if not field:
        raise ValueError(f'A {name} must be provided.')
