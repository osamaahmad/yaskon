from django.urls import path
from . import views

app_name = 'user'

urlpatterns = [
    path('create/', views.UserCreateAPIView.as_view(), name='create'),
    path('list/', views.UserListAPIView.as_view(), name='list'),
    path('update/', views.UserProfileUpdateAPIView.as_view(), name='update'),
    path('token/', views.UserTokenAPIView.as_view(), name='token'),
]