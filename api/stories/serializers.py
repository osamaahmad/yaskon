from rest_framework import serializers
from .models import Story, SeenStory


class StorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Story
        fields = ('id', 'user', 'image', 'caption', 'creation_date', 'expiration_date')
        read_only_fields = ('id', 'user', 'creation_date')

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['user'] = user
        return Story.objects.create(**validated_data)


class SeenStorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SeenStory
        fields = ('story',)

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['user'] = user
        # get_or_create function returns tuple (instance, created)
        return SeenStory.objects.get_or_create(**validated_data)[0]
