from django.shortcuts import redirect
from rest_framework import generics, permissions, authentication
from .serializers import StorySerializer, SeenStorySerializer
from .models import Story, SeenStory
from django.utils import timezone
import api


def filter_expired_stories():
    # TODO is this the most efficient way?
    expired_stories = Story.objects.filter(expiration_date__lte=timezone.now())
    for story in expired_stories:
        SeenStory.objects.filter(story=story).delete()
    expired_stories.delete()


class StoryCreateAPIView(generics.CreateAPIView):
    serializer_class = StorySerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]


class UserStoriesListAPIView(generics.ListAPIView):
    serializer_class = StorySerializer

    def get_queryset(self):
        filter_expired_stories()
        user_pk = self.kwargs['pk']
        return Story.objects.filter(user=user_pk)


class CurrentUserStoriesListAPIView(generics.ListAPIView):
    serializer_class = StorySerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    def get_queryset(self):
        filter_expired_stories()
        return Story.objects.filter(user=self.request.user)


class AllUsersStoriesListAPIView(generics.ListAPIView):
    serializer_class = StorySerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    def get_queryset(self):
        filter_expired_stories()
        return Story.objects.all()


class StoryListAPIView(generics.ListAPIView):
    serializer_class = StorySerializer

    def get_queryset(self):
        story_pk = self.kwargs['pk']
        return Story.objects.filter(id=story_pk)


class StoryUpdateAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = StorySerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    def get_queryset(self):
        story_pk = self.kwargs['pk']
        return Story.objects.filter(id=story_pk)


class SeenStoryCreateAPIView(generics.CreateAPIView):
    serializer_class = SeenStorySerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]


class SeenStoryDestroyAPIView(generics.DestroyAPIView):
    serializer_class = SeenStorySerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    def get_object(self):
        story_pk = self.kwargs['pk']
        return SeenStory.objects.filter(story=story_pk)


class SeenStoriesListAPIView(generics.ListAPIView):
    serializer_class = SeenStorySerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    def get_queryset(self):
        filter_expired_stories()
        return SeenStory.objects.filter(user=self.request.user)

def story_image_url(request, id):
    story = Story.objects.get(id=id)
    url = 'https://' + request.META['HTTP_HOST'] + api.settings.MEDIA_URL + str(story.image)
    return redirect(url)
