from django.contrib import admin
from .models import Story, SeenStory

admin.site.register(Story)
admin.site.register(SeenStory)
