# Generated by Django 3.2 on 2021-05-07 02:03

import core.functions
from django.db import migrations, models
import django.utils.timezone
import stories.models


class Migration(migrations.Migration):

    dependencies = [
        ('stories', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='story',
            name='creation_date',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='story',
            name='image',
            field=models.ImageField(upload_to=core.functions.UploadImage('upload/stories', stories.models.story_folder_name_getter)),
        ),
    ]
