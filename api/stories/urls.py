from django.urls import path
from . import views

app_name = 'stories'

urlpatterns = [
    path('', views.CurrentUserStoriesListAPIView.as_view(), name='list_my_stories'),
    path('all/', views.AllUsersStoriesListAPIView.as_view(), name='list_all_stories'),
    path('create/', views.StoryCreateAPIView.as_view(), name='create'),
    path('<int:pk>/update/', views.StoryUpdateAPIView.as_view(), name='update'),
    path('<int:pk>/', views.StoryListAPIView.as_view(), name='view'),
    path('user/<int:pk>/', views.UserStoriesListAPIView.as_view(), name='list_user_stories'),
    path('see/', views.SeenStoryCreateAPIView.as_view(), name='see_story'),
    path('unsee/<int:pk>/', views.SeenStoryDestroyAPIView.as_view(), name='unsee_story'),
    path('seen/', views.SeenStoriesListAPIView.as_view(), name='list_seen_stories'),
    path('<int:id>/image/', views.story_image_url, name='story_image_url'),
]
