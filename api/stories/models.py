from django.db import models
from api import settings
from core.functions import UploadImage
from .settings import STORY_UPLOAD_PATH


def story_folder_name_getter(instance):
    return instance.user.username


get_story_upload_path = UploadImage(STORY_UPLOAD_PATH, story_folder_name_getter)


class Story(models.Model):

    def __str__(self):
        return self.user.username + ':' + str(self.id) + ' (' + self.caption + ')'

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    image = models.ImageField(upload_to=get_story_upload_path)
    caption = models.TextField()
    creation_date = models.DateTimeField(auto_now_add=True)
    expiration_date = models.DateTimeField()


# TODO may change the way it's done.
class SeenStory(models.Model):

    def __str__(self):
        return str(self.story)

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    story = models.ForeignKey(Story, on_delete=models.CASCADE)
    # TODO add the time where the story had been watched.
