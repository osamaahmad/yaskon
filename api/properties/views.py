from functools import reduce

from django.shortcuts import redirect
from rest_framework import generics, permissions, authentication
from .models import PropertyImage, PropertyRating, Amenity, Property, PropertyPreference
from .serializers import PropertyImageSerializer, PropertyRatingSerializer, AmenitySerializer, \
    PropertySerializer, PropertyPreferenceSerializer
from .settings import PROPERTIES_SIMILARITY_STR_FIELDS_MULTIPLIERS, PROPERTIES_SIMILARITY_NUM_FIELDS_MULTIPLIERS
from core.functions import calculate_str_similarity, calculate_num_similarity
from django.db.models import Q, Count
import operator

import api


# TODO refactor duplicate code...


class PropertyRatingCreateAPIView(generics.CreateAPIView):
    # TODO only the users that have tried the service should be able to rate.

    serializer_class = PropertyRatingSerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]


class PropertyRatingsListAPIView(generics.ListAPIView):
    serializer_class = PropertyRatingSerializer

    def get_queryset(self):
        property_pk = self.kwargs['pk']
        return PropertyRating.objects.filter(property=property_pk)


class PropertyRatingUpdateAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PropertyRatingSerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    def get_queryset(self):
        rating_pk = self.kwargs['pk']
        return PropertyRating.objects.filter(id=rating_pk)


class PropertyImageCreateAPIView(generics.CreateAPIView):
    serializer_class = PropertyImageSerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]


class PropertyImageListAPIView(generics.ListAPIView):
    serializer_class = PropertyImageSerializer

    def get_queryset(self):
        image_pk = self.kwargs['pk']
        return PropertyImage.objects.filter(id=image_pk)


class PropertyImageUpdateAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PropertyImageSerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    def get_queryset(self):
        image_pk = self.kwargs['pk']
        return PropertyImage.objects.filter(id=image_pk)


class AmenityCreateAPIView(generics.CreateAPIView):
    serializer_class = AmenitySerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]


class AmenityListAPIView(generics.ListAPIView):
    serializer_class = AmenitySerializer

    def get_queryset(self):
        amenity_pk = self.kwargs['pk']
        return Amenity.objects.filter(id=amenity_pk)


class AmenityUpdateAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = AmenitySerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    def get_queryset(self):
        amenity_pk = self.kwargs['pk']
        return Amenity.objects.filter(id=amenity_pk)


class PropertyCreateAPIView(generics.CreateAPIView):
    serializer_class = PropertySerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]


class PropertyListAPIView(generics.ListAPIView):
    serializer_class = PropertySerializer

    def get_queryset(self):
        property_pk = self.kwargs['pk']
        return Property.objects.filter(id=property_pk)


class PropertyUpdateAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PropertySerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    def get_queryset(self):
        property_pk = self.kwargs['pk']
        return Property.objects.filter(id=property_pk)


class UserPropertiesListAPIView(generics.ListAPIView):
    serializer_class = PropertySerializer

    def get_queryset(self):
        user_pk = self.kwargs['pk']
        return Property.objects.filter(user=user_pk)


class AllUsersPropertiesListAPIView(generics.ListAPIView):
    serializer_class = PropertySerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    def get_queryset(self):
        return get_properties_sorted_by_preferences(
            Property.objects.all(),
            self.request.user
        )


class CurrentUserPropertiesListAPIView(generics.ListAPIView):
    serializer_class = PropertySerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    def get_queryset(self):
        return Property.objects.filter(user=self.request.user)


def get_lte_gte_params(key):
    return {
        'min_' + key: [key + '__gte'],
        'max_' + key: [key + '__lte']
    }


class PropertySearchAPIView(generics.ListAPIView):
    serializer_class = PropertySerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    direct_queries_params = {
        'contains': ['title__icontains', 'description__icontains', 'location__icontains'],
        'user': ['user'],
        'title': ['title__icontains'],
        # no filtering by images for now.
        'description': ['description__icontains'],
        # price should be greater than or equal to min_price, not max_price.
        **get_lte_gte_params('price'),
        'payment_type': ['payment_type__icontains'],
        'location': ['location__icontains'],
        'city': ['city__icontains'],
        # TODO improve the filtering by dates.
        **get_lte_gte_params('post_date'),
        **get_lte_gte_params('floor_count'),
        **get_lte_gte_params('bathrooms_count'),
        **get_lte_gte_params('bedrooms_count'),
        **get_lte_gte_params('beds_count'),
        # should the next 2 be an exact match?
        'payment_method': ['payment_method__icontains'],
        'property_type': ['property_type__icontains'],
        **get_lte_gte_params('broker_percentage'),
        'is_green_house': ['is_green_house'],
    }

    class IndirectQueryObject:
        def __init__(self, key, query_class, field_name, args):
            self.key = key
            self.query_class = query_class
            self.field_name = field_name
            self.args = args

    indirect_queries_params = [
        IndirectQueryObject(
            key='amenities',
            query_class=Amenity,
            field_name='amenities',
            args=['title__icontains', 'caption__icontains']
        ),
    ]

    def get_parameter(self, key):
        return self.request.query_params.get(key)

    def create_query(self, value, operation, *args):
        if value:
            intermediate_queries = []
            for arg in args:
                kwargs = {arg: value}
                intermediate_queries.append(Q(**kwargs))
            if len(intermediate_queries) > 0:
                result_query = reduce(operation, intermediate_queries)
                return result_query
        return None

    def get_direct_queries(self, keys):
        result = []
        for key in keys:
            value = self.get_parameter(key)
            if not value:
                continue
            args = keys[key]
            query = self.create_query(value, operator.or_, *args)
            if query:
                result.append(query)
        return result

    def filter_by_indirect_query(self, queryset, indirect_query):
        params = self.get_parameter(indirect_query.key)
        if params:
            values = []
            params = params.split(',')
            for param in params:
                temp_query = self.create_query(param, operator.or_, *indirect_query.args)
                temp_result = indirect_query.query_class.objects.filter(temp_query)
                if len(temp_result) == 0:
                    return Property.objects.none()  # no match
                values.extend(temp_result)

            field = indirect_query.field_name
            in_str = field + '__in'
            count_str = 'count_' + field

            kwargs = {in_str: values}
            queryset = queryset.filter(**kwargs)
            kwargs = {count_str: Count(field)}
            queryset = queryset.annotate(**kwargs)
            kwargs = {count_str: len(values)}
            queryset = queryset.filter(**kwargs)

        return queryset

    def filter_by_indirect_queries(self, queryset, indirect_queries):
        for query in indirect_queries:
            queryset = self.filter_by_indirect_query(queryset, query)
        return queryset

    def get_queryset(self):

        return_empty_set = len(self.indirect_queries_params) == 0
        queryset = Property.objects.all()

        direct_queries = self.get_direct_queries(self.direct_queries_params)
        if len(direct_queries) > 0:
            return_empty_set = False
            query = reduce(operator.and_, direct_queries)
            queryset = queryset.filter(query)

        queryset = self.filter_by_indirect_queries(queryset, self.indirect_queries_params)

        if return_empty_set:
            return Property.objects.none()

        return get_properties_sorted_by_preferences(
            queryset.distinct(),
            self.request.user
        )


class PropertyPreferenceCreateAPIView(generics.ListCreateAPIView):
    serializer_class = PropertyPreferenceSerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    def get_queryset(self):
        return PropertyPreference.objects.filter(user=self.request.user)


class PropertyPreferenceUpdateAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PropertyPreferenceSerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    def get_queryset(self):
        preference_pk = self.kwargs['pk']
        return PropertyPreference.objects.filter(id=preference_pk)


def image_id_to_url(request, id):
    image = PropertyImage.objects.get(id=id)
    url = 'https://' + request.META['HTTP_HOST'] + api.settings.MEDIA_URL + str(image.image)
    return redirect(url)


def calculate_properties_fields_similarity(a, b, fields_multipliers, similarity_func):
    similarity = 0
    multipliers_sum = 0
    for field in fields_multipliers:
        multiplier = fields_multipliers[field]
        multipliers_sum += multiplier
        similarity += similarity_func(getattr(a, field), getattr(b, field))
    similarity /= multipliers_sum
    return similarity


def calculate_properties_similarity(a, b):
    str_similarity = calculate_properties_fields_similarity(
        a,
        b,
        PROPERTIES_SIMILARITY_STR_FIELDS_MULTIPLIERS,
        calculate_str_similarity
    )
    num_similarity = calculate_properties_fields_similarity(
        a,
        b,
        PROPERTIES_SIMILARITY_NUM_FIELDS_MULTIPLIERS,
        calculate_num_similarity
    )
    return (float(str_similarity) + float(num_similarity)) / 2


def calculate_property_max_similarity_with_preferences(property, preferences):
    max_similarity = 0
    for preference in preferences:
        max_similarity = max(max_similarity, calculate_properties_similarity(property, preference.property))
    return max_similarity


def get_properties_sorted_by_preferences(properties, user):
    preferences = PropertyPreference.objects.filter(user=user)

    def key_func(property):
        return calculate_property_max_similarity_with_preferences(
            property,
            preferences
        )

    result = sorted(properties, key=key_func, reverse=True)
    return result
