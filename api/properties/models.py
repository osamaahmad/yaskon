from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

from api import settings
from properties.settings import AMENITY_TITLE_MAX_LENGTH, PROPERTY_TITLE_MAX_LENGTH, PRICE_DEFAULT, PRICE_MAX_DIGITS, \
    PRICE_DECIMAL_PLACES, PROPERTY_RATING_MIN_VALUE, PROPERTY_RATING_MAX_VALUE, PROPERTY_RATING_DEFAULT, \
    FLOORS_COUNT_DEFAULT, PAYMENT_METHOD_MAX_CHARS_LEN, PROPERTY_TYPE_MAX_CHARS_LEN, BROKER_PERCENTAGE_DEFAULT, \
    BROKER_PERCENTAGE_DECIMAL_PLACES, BROKER_PERCENTAGE_MAX_DIGITS, IS_GREEN_HOUSE_DEFAULT, PAYMENT_TYPE_MAX_CHARS_LEN,\
    BATHROOMS_COUNT_DEFAULT, BEDROOMS_COUNT_DEFAULT, BEDS_COUNT_DEFAULT, AREA_DEFAULT, AREA_MAX_DIGITS, AREA_DECIMAL_PLACES,\
    property_image_upload_func

# TODO check what arguments to pass the TextFields.
# TODO may introduce videos along with images.


class PropertyImage(models.Model):

    def __str__(self):
        return self.caption

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    image = models.ImageField(upload_to=property_image_upload_func)
    caption = models.TextField(blank=True)


class Amenity(models.Model):

    def __str__(self):
        return self.title

    title = models.CharField(max_length=AMENITY_TITLE_MAX_LENGTH)
    images = models.ManyToManyField('PropertyImage', blank=True)
    caption = models.TextField(blank=True)


class PropertyRating(models.Model):
    def __str__(self):
        return str(self.user) + ' : ' + str(self.property) + ' : ' + str(self.rating)

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    property = models.ForeignKey('Property', on_delete=models.CASCADE)
    rating = models.IntegerField(
        default=PROPERTY_RATING_DEFAULT,
        validators=[
            MaxValueValidator(PROPERTY_RATING_MAX_VALUE),
            MinValueValidator(PROPERTY_RATING_MIN_VALUE)
        ]
    )


class Property(models.Model):

    def __str__(self):
        return self.title

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=PROPERTY_TITLE_MAX_LENGTH)
    images = models.ManyToManyField('PropertyImage', blank=True)
    description = models.TextField(blank=True)
    area = models.DecimalField(
        default=AREA_DEFAULT,
        max_digits=AREA_MAX_DIGITS,
        decimal_places=AREA_DECIMAL_PLACES
    )
    price = models.DecimalField(
        default=PRICE_DEFAULT,
        max_digits=PRICE_MAX_DIGITS,
        decimal_places=PRICE_DECIMAL_PLACES
    )
    payment_type = models.CharField(max_length=PAYMENT_TYPE_MAX_CHARS_LEN)
    location = models.TextField(blank=True)  # TODO may improve this?
    city = models.TextField(blank=True)  # TODO may improve this?
    amenities = models.ManyToManyField('Amenity', blank=True)
    post_date = models.DateTimeField(auto_now_add=True)
    floors_count = models.IntegerField(default=FLOORS_COUNT_DEFAULT, blank=True)
    bathrooms_count = models.IntegerField(default=BATHROOMS_COUNT_DEFAULT, blank=True)
    bedrooms_count = models.IntegerField(default=BEDROOMS_COUNT_DEFAULT, blank=True)
    beds_count = models.IntegerField(default=BEDS_COUNT_DEFAULT, blank=True)
    payment_method = models.CharField(max_length=PAYMENT_METHOD_MAX_CHARS_LEN, blank=True)  # TODO use something like enums instead.
    property_type = models.CharField(max_length=PROPERTY_TYPE_MAX_CHARS_LEN, blank=True)  # TODO use something like enums instead.
    broker_percentage = models.DecimalField(
        default=BROKER_PERCENTAGE_DEFAULT,
        decimal_places=BROKER_PERCENTAGE_DECIMAL_PLACES,
        max_digits=BROKER_PERCENTAGE_MAX_DIGITS
    )
    is_green_house = models.BooleanField(default=IS_GREEN_HOUSE_DEFAULT)


class PropertyPreference(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    property = models.ForeignKey('Property', on_delete=models.CASCADE)

# class Payment(enum.Enum):
#     OTHER = 0
#     FOR_SALE = 1 #     DAILY = 2
#     WEEKLY = 3
#     MONTHLY = 4
#     ANNUAL = 5
#
#
# class PropertyType(enum.Enum):
#     pass
