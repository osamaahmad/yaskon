from django.urls import path
from . import views

app_name = 'properties'

urlpatterns = [
    path('', views.CurrentUserPropertiesListAPIView.as_view(), name='list_my_properties'),
    path('all/', views.AllUsersPropertiesListAPIView.as_view(), name='list_all_properties'),
    path('create/', views.PropertyCreateAPIView.as_view(), name='create'),
    path('<int:pk>/update/', views.PropertyUpdateAPIView.as_view(), name='update'),
    path('<int:pk>/', views.PropertyListAPIView.as_view(), name='view'),
    path('<int:pk>/ratings', views.PropertyRatingsListAPIView.as_view(), name='view_property_ratings'),
    path('user/<int:pk>/', views.UserPropertiesListAPIView.as_view(), name='list_user_properties'),
    path('amenity/create/', views.AmenityCreateAPIView.as_view(), name='create_amenity'),
    path('amenity/<int:pk>/update/', views.AmenityUpdateAPIView.as_view(), name='update_amenity'),
    path('amenity/<int:pk>/', views.AmenityListAPIView.as_view(), name='view_amenity'),  # TODO remove this
    path('image/create/', views.PropertyImageCreateAPIView.as_view(), name='create_property_image'),
    path('image/<int:pk>/update/', views.PropertyImageUpdateAPIView.as_view(), name='update_property_image'),
    path('image/<int:pk>/', views.PropertyImageListAPIView.as_view(), name='view_property_image'),  # TODO remove this
    path('rating/create/', views.PropertyRatingCreateAPIView.as_view(), name='create_property_rating'),
    path('rating/<int:pk>/update/', views.PropertyRatingUpdateAPIView.as_view(), name='update_property_rating'),
    path('search/', views.PropertySearchAPIView.as_view(), name='property_search'),
    path('preferences/', views.PropertyPreferenceCreateAPIView.as_view(), name='create_list_preferences'),
    path('preferences/<int:pk>/', views.PropertyPreferenceUpdateAPIView.as_view(), name='update_preference'),
    path('image/to_url/<int:id>/', views.image_id_to_url, name='image_id_to_url'),
]
