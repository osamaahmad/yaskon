from django.contrib.auth import get_user_model
from rest_framework import serializers
from .models import PropertyImage, PropertyRating, Amenity, Property, PropertyPreference


class PropertyImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = PropertyImage
        fields = ('id', 'user', 'image', 'caption')
        read_only_fields = ('id', 'user')

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['user'] = user
        return super(PropertyImageSerializer, self).create(validated_data)


class PropertyRatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = PropertyRating
        fields = ('id', 'user', 'property', 'rating')
        read_only_fields = ('id', 'user')

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['user'] = user
        property = validated_data['property']

        rating = PropertyRating.objects.filter(user=user, property=property)
        if rating.exists():
            # rating is a queryset currently.
            assert(len(rating) == 1)
            rating.update(**validated_data)
            rating = rating.first()
            return rating

        return super(PropertyRatingSerializer, self).create(validated_data)


class AmenitySerializer(serializers.ModelSerializer):
    # TODO should amenities have a user?
    class Meta:
        model = Amenity
        fields = ('id', 'title', 'images', 'caption')
        read_only_fields = ('id',)
        images = PropertyImageSerializer(many=True, read_only=False)


class PropertySerializer(serializers.ModelSerializer):
    class Meta:
        model = Property
        fields = ('id', 'user', 'title', 'images', 'description', 'area', 'price', 'payment_type',
                  'location', 'city', 'amenities', 'post_date', 'rating', 'floors_count',
                  'bathrooms_count', 'bedrooms_count', 'beds_count', 'payment_method',
                  'property_type', 'broker_percentage', 'is_green_house')
        read_only_fields = ('id', 'user', 'post_date')

    rating = serializers.SerializerMethodField()
    # TODO this is just temporary for the project. later, remove this line.
    amenities = serializers.SlugRelatedField(
        many=True,
        read_only=False,
        slug_field='title',
        queryset=Amenity.objects.all()
    )

    def get_rating(self, obj):
        # TODO improve this... this calculates for each serialization...
        # TODO you might update when a new rating is created.

        ratings = PropertyRating.objects.filter(property=obj.id)

        denominator = len(ratings)
        if denominator == 0:
            return 0

        sum = 0
        for rating in ratings:
            sum += rating.rating

        return sum / denominator

    def create(self, validated_data):
        if validated_data.get('user') is None:
            user = self.context['request'].user
            validated_data['user'] = user
        return super(PropertySerializer, self).create(validated_data)


class PropertyPreferenceSerializer(serializers.ModelSerializer):
    class Meta:
        model = PropertyPreference
        fields = ('id', 'user', 'property')
        read_only_fields = ('id', 'user')

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['user'] = user
        return super(PropertyPreferenceSerializer, self).create(validated_data)
