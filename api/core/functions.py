import datetime
import uuid
import os
from django.utils.deconstruct import deconstructible

import nltk
from nltk.corpus import stopwords as nltk_stopwords
from nltk.tokenize import word_tokenize


def default_folder_name_getter(instance):
    return instance.username


@deconstructible
class UploadImage:

    def __init__(self, upload_path, folder_name_getter=default_folder_name_getter):
        self.upload_path = upload_path
        self.folder_name_getter = folder_name_getter

    def __call__(self, instance, filename):
        filename = f'{datetime.datetime.now()}_{uuid.uuid4()}_{filename}'
        return os.path.join(self.upload_path, self.folder_name_getter(instance), filename)

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.upload_path == other.upload_path


def calculate_num_similarity(a, b):

    if max(a, b) == 0:
        a += 1
        b += 1

    min_val = min(a, b)
    if min_val < 0:
        a -= min_val
        b -= min_val

    return min(a, b) / max(a, b)


def calculate_str_similarity(str_a, str_b):

    # TODO should this be removed? should it be
    #  here to keep it up-to-date?
    nltk.download('stopwords')
    nltk.download('punkt')

    a_list = word_tokenize(str(str_a))
    b_list = word_tokenize(str(str_b))

    stopwords = nltk_stopwords.words('english')

    a_unique = {word for word in a_list if word not in stopwords}
    b_unique = {word for word in b_list if word not in stopwords}

    words_vector = a_unique.union(b_unique)

    # 1 if word is found in words_vector, 0 otherwise.
    a_word_exists = []
    b_word_exists = []

    for word in words_vector:
        a_word_exists.append(1 if word in a_unique else 0)
        b_word_exists.append(1 if word in b_unique else 0)

    common_words_count = 0
    for i in range(len(words_vector)):
        common_words_count += a_word_exists[i] * b_word_exists[i]

    a_sum = sum(a_word_exists)
    b_sum = sum(b_word_exists)

    if a_sum == 0 or b_sum == 0:
        return 0

    cosine_similarity = common_words_count / float(a_sum * b_sum) ** 0.5
    return cosine_similarity
