# Base Image
FROM python:3.8

# create and set working directory
RUN mkdir /app
WORKDIR /app

# Install system dependencies
RUN apt update && apt install -y --no-install-recommends \
        tzdata \
        python3-setuptools \
        python3-pip \
        python3-dev \
        python3-venv \
        git \
        && \
    apt clean && \
    rm -rf /var/lib/apt/lists/*

ADD ./requirements.txt /app/requirements.txt

RUN pip3 install -r requirements.txt

# Add current directory code to working directory
ADD . /app/

ENV PORT=8000

EXPOSE $PORT 

RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static

CMD cd api && \
    python3 manage.py makemigrations && \
    python3 manage.py migrate && \
    python3 manage.py runserver 0.0.0.0:$PORT
